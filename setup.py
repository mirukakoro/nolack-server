# setup.py
"""Setup for Nolack Server"""
try:
  from nolack_server import __version__
except ImportError as err:
  __version__ = 'unknown'

from setuptools import (setup, find_packages)

setup(name='nolack-server',
      version=__version__,
      description='Nolack backend server.',
      author='@colourdelete, @keidaroo, <hanamiitagaki.design@gmail.com>',
      packages=find_packages(exclude=[
          "*.tests", "*.tests.*", "tests.*", "tests", "log", "log.*", "*.log",
          "*.log.*"
      ]),
      install_requires=[
          'requests',
          'google-api-python-client',
          'google-auth-httplib2',
          'google-auth-oauthlib',
      ])
