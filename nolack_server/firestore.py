import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore
import os
import datetime
from dateutil.parser import parse as parse_datetime
from . import log
logger = log.logger
import google
# TODO: implement error catching for google.api_core.exceptions.ResourceExhausted (429 Quota exceeded.)


class firestore_wrapper():
  def __init__(
      self,
      config,
      service_account_json_path='/opt/nolack/nolack-server/config/service-account.json',
  ):
    self.service_account_json_path = service_account_json_path
    self.config = config
    self.cred = credentials.Certificate(service_account_json_path)
    firebase_admin.initialize_app(self.cred)
    self.db = firestore.client()
    self._accessed_count_day = 0
    self._start_day = int(self.get_now().day)
    self._demands = None
    self._demands_last_accessed = datetime.datetime(
        1,
        1,
        1,
    ) # 0001-01-01 ISO8601 time
    self.demands = None
    self._commands = None
    self._commands_last_accessed = datetime.datetime(
        1,
        1,
        1,
    )
    self.commands = None
    self._subscriptions = None
    self._subscriptions_last_accessed = datetime.datetime(
        1,
        1,
        1,
    )
    self.subscriptions = None
    self._messages = None
    self._messages_last_accessed = datetime.datetime(
        1,
        1,
        1,
    )
    self.messages = None

  def _accessed_db(self, ):
    now_day = int(self.get_now().day)
    if now_day != self._start_day:
      self._start_day = now_day
      self._accessed_count_day = 1
      logger.debug(f'Reset access count. ({now_day} is not {self._start_day})')
    else:
      logger.debug(f'Increment access count. ({now_day} is {self._start_day})')
      self._accessed_count_day += 1
    logger.debug(
        f'Firestore accessed {self._accessed_count_day} time(s) today. (UTC 00:00 <= time > 24:00'
    )

  def _get_demands(
      self,
      cache_limit=None,
  ): # TODO: Cache works, but doesn;t update demands last accessed! FIX THIS!!!
    'Gets raw collection object.'
    assert isinstance(cache_limit, datetime.timedelta) or cache_limit == None
    if cache_limit == None:
      cache_limit = datetime.timedelta(minutes=int(
          self.config['firestore_config']['cache_limit']['demands']))
    now_time = self.get_now()
    if (self._demands_last_accessed +
        cache_limit).replace(tzinfo=datetime.timezone.utc) < now_time.replace(
            tzinfo=datetime.timezone.utc):
      logger.debug(
          'Cache Limit for demands reached; accessing... ({} < {})'.format(
              (self._demands_last_accessed +
               cache_limit).replace(tzinfo=datetime.timezone.utc),
              now_time.replace(tzinfo=datetime.timezone.utc)))
      self._demands = self.db.collection('demands')
      self._accessed_db()
      self._demands_last_accessed = now_time.replace(
          tzinfo=datetime.timezone.utc)
    else:
      logger.debug('Cache Limit for demands not reached; using cache...')
    return self._demands

  def get_demands(self, ):
    'Gets demands.'
    self._get_demands()
    self.demands = self._demands.stream()
    return self.demands

  def map_demands(
      self,
      func,
      get=True,
  ):
    'Maps each demand to func with kwargs of `demand_id`, `demand_dict`.'
    if get: self.get_demands()
    results = []
    for demand in self.demands:
      result = func(demand_id=demand.id, demand_dict=demand.to_dict())
      results.append(result)
    return results

  def _get_commands(
      self,
      cache_limit=None,
  ):
    'Gets raw collection object.'
    assert isinstance(cache_limit, datetime.timedelta) or cache_limit == None
    if cache_limit == None:
      cache_limit = datetime.timedelta(minutes=int(
          self.config['firestore_config']['cache_limit']['commands']))
    now_time = self.get_now()
    if (self._commands_last_accessed +
        cache_limit).replace(tzinfo=datetime.timezone.utc) < now_time.replace(
            tzinfo=datetime.timezone.utc):
      logger.debug('Cache Limit for commands reached; accessing...')
      self._commands = self.db.collection('commands')
      self._accessed_db()
      self._commands_last_accessed = now_time.replace(
          tzinfo=datetime.timezone.utc)
    else:
      logger.debug('Cache Limit for commands not reached; using cache...')
    return self._commands

  def get_commands(self, ):
    'Gets commands.'
    self._get_commands()
    self.commands = self._commands.stream()
    return self.commands

  def map_commands(
      self,
      func,
      get=True,
  ):
    'Maps each demand to func with kwargs of `command_id`, `command_dict`.'
    if get: self.get_commands()
    results = []
    for command in self.commands:
      result = func(command_id=command.id, command_dict=command.to_dict())
      results.append(result)
    return results

  def get_applicable_commands(
      self,
      id_=None,
      name=None,
  ):
    assert id_ != None or name != None

    def is_applicable_command(
        command_id,
        command_dict,
    ):
      if (command_dict['target']['id'] == id_ or command_dict['target']['name']
          == name) and command_dict['done'] == False:
        return True, command_id, command_dict
      else:
        return False, None, None

    results = self.map_commands(is_applicable_command, )
    commands = []
    for result in results:
      if result[0]:
        command = result[2]
        command['_id'] = result[1]
        commands.append(command)
    return commands

  def _get_subscriptions(
      self,
      cache_limit=None,
  ):
    'Gets raw collection object.'
    assert isinstance(cache_limit, datetime.timedelta) or cache_limit == None
    if cache_limit == None:
      cache_limit = datetime.timedelta(minutes=int(
          self.config['firestore_config']['cache_limit']['subscriptions']))
    now_time = self.get_now()
    if (self._subscriptions_last_accessed +
        cache_limit).replace(tzinfo=datetime.timezone.utc) < now_time.replace(
            tzinfo=datetime.timezone.utc):
      logger.debug('Cache Limit for subscriptions reached; accessing...')
      self._subscriptions = self.db.collection('subscriptions')
      self._accessed_db()
      self._subscriptions_last_accessed = now_time.replace(
          tzinfo=datetime.timezone.utc)
    else:
      logger.debug('Cache Limit for subscriptions not reached; using cache...')
    return self._subscriptions

  def get_subscriptions(
      self,
      type_,
  ):
    'Gets subscriptions.'
    assert type_ in ('mail', 'line', None)
    self._get_subscriptions()
    type_filtered = []
    for subscription in self._subscriptions.stream():
      subscription_id = subscription.id
      subscription_dict = subscription.to_dict()
      if 'type' not in subscription_dict.keys():
        continue
      if 'confirmSent' not in subscription_dict.keys():
        continue
      if 'confirmOkByUser' not in subscription_dict.keys():
        continue
      if type_ == None or subscription_dict['type'] == type_:
        type_filtered.append(subscription)
    self.subscriptions = type_filtered
    return type_filtered

  def set_subscription_last_sent(
      self,
      id_,
      last_sent,
  ):
    if not self.is_date(last_sent):
      last_sent = last_sent.replace(tzinfo=datetime.timezone.utc).isoformat()
    return self.set_subscription_attr(id_, 'lastSent', last_sent)

  def get_subscription_last_sent(
      self,
      id_,
  ):
    return parse_datetime(self.get_subscription_attr(
        id_,
        'lastSent',
    ))

  def set_subscription_confirm_sent(
      self,
      id_,
      confirm_sent,
  ):
    if type(confirm_sent) != type(True):
      confirm_sent = bool(confirm_sent)
    return self.set_subscription_attr(id_, 'confirmSent', confirm_sent)

  def get_subscription_confirm_sent(
      self,
      id_,
  ):
    return bool(self.get_subscription_attr(
        id_,
        'confirmSent',
    ))

  def get_subscription_confirm_ok_by_user(
      self,
      id_,
  ):
    return bool(self.get_subscription_attr(
        id_,
        'confirmOkByUser',
    ))

  def set_subscription_attr(
      self,
      id_,
      attr_key,
      attr_value,
  ):
    for subscription in self.db.collection('subscriptions').stream():
      if id_ == subscription.id:
        old = subscription.to_dict()
        old[attr_key] = attr_value
        self.db.collection('subscriptions').document(id_).set(old)
        return True
    raise RuntimeError(f'ID {id_} not found in subscriptions.')

  def get_subscription_attr(
      self,
      id_,
      attr_key,
  ):
    for subscription in self.db.collection('subscriptions').stream():
      if id_ == subscription.id:
        old = subscription.to_dict()
    return old[attr_key]

  def map_subscriptions(
      self,
      func,
      type_,
      get=True,
  ):
    'Maps each demand to func with kwargs of `subscription_id`, `subscription_dict`.'
    if get: self.get_subscriptions(type_, )
    results = []
    for subscription in self.subscriptions:
      result = func(subscription_id=subscription.id,
                    subscription_dict=subscription.to_dict())
      results.append(result)
    return results

  def _get_messages(
      self,
      cache_limit=None,
  ):
    'Gets raw collection object.'
    assert isinstance(cache_limit, datetime.timedelta) or cache_limit == None
    if cache_limit == None:
      cache_limit = datetime.timedelta(minutes=int(
          self.config['firestore_config']['cache_limit']['messages']))
    now_time = self.get_now()
    if (self._messages_last_accessed +
        cache_limit).replace(tzinfo=datetime.timezone.utc) < now_time.replace(
            tzinfo=datetime.timezone.utc):
      logger.debug('Cache Limit for commands reached; accessing...')
      self._messages = self.db.collection('messages')
      self._accessed_db()
      self._messages_last_accessed = now_time.replace(
          tzinfo=datetime.timezone.utc)
    else:
      logger.debug('Cache Limit for messages not reached; using cache...')
    return self._messages

  def get_messages(
      self,
      type_,
  ):
    'Gets messages.'
    assert type_ in ('mail', 'line', None)
    self._get_messages()
    type_filtered = []
    for message in self._messages.stream():
      message_id = message.id
      message_dict = message.to_dict()
      if type_ == None or message_dict['type'] == type_:
        type_filtered.append(message)
    self.messages = type_filtered
    return self.messages

  def map_messages(
      self,
      func,
      type_,
      get=True,
  ):
    'Maps each demand to func with kwargs of `message_id`, `message_dict`.'
    if get: self.get_messages(type_)
    results = []
    for message in self.messages:
      result = func(message_id=message.id, message_dict=message.to_dict())
      results.append(result)
    return results

  def get_isonow(self, ):
    return self.get_now().isoformat()

  def get_now(self, ):
    return datetime.datetime.utcnow().replace(tzinfo=datetime.timezone.utc)

  def is_date(self, string, fuzzy=False):
    '''
        Return whether the string can be interpreted as a date.

        :param string: str, string to check for date
        :param fuzzy: bool, ignore unknown tokens in string if True
        '''
    try:
      parse_datetime(string, fuzzy=fuzzy)
      return True
    except Exception as err:
      return False

  def get_applicable_messages(
      self,
      type_,
  ):
    def is_applicable_message(
        message_id,
        message_dict,
    ):
      nowtime = self.get_now()
      use_time_start = message_dict['useTimeStart']
      use_time_end = message_dict['useTimeEnd']
      assert self.is_date(use_time_start)
      assert self.is_date(use_time_end)
      use_time_start = parse_datetime(use_time_start)
      use_time_end = parse_datetime(use_time_end)
      if use_time_start <= nowtime and use_time_end >= nowtime:
        return True, message_id, message_dict
      else:
        return False, None, None

    results = self.map_messages(
        is_applicable_message,
        type_,
    )
    messages = []
    for result in results:
      if result[0]:
        message = result[2]
        message['id'] = result[1]
        messages.append(message)
    return messages

  def get_applicable_message(
      self,
      type_,
  ):
    messages = self.get_applicable_messages(type_, )
    max_priority = 0
    max_priority_value = None
    for message in messages:
      if message['priority'] > max_priority:
        max_priority_value = message
    return max_priority_value
