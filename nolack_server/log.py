import logging
import datetime

logging.basicConfig(filename='example.log', level=logging.DEBUG)
formatter = logging.Formatter(
    "%(asctime)s %(threadName)-12.12s %(levelname)-5.5s %(message)s")
logger = logging.getLogger()

fileHandler = logging.FileHandler("{0}/{1}.log".format(
    '.',
    f'nolack_server_{str(datetime.datetime.now().isoformat())}.log',
))
fileHandler.setFormatter(formatter)
logger.addHandler(fileHandler)

consoleHandler = logging.StreamHandler()
consoleHandler.setFormatter(formatter)
logger.addHandler(consoleHandler)
