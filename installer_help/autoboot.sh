#!/usr/bin/bash
set -e

echo "Nolack Server Runner Script"
echo 'Version <ci_commit_sha> (<ci_commit_tag>)'
echo '        <ci_commit_title>'
if [ "$EUID" -ne 0 ]
  then echo "Please run the runner as root."
  exit
fi

echo "Going to path..."
cd '/opt/nolack/nolack-server/<ci_commit_sha>/nolack-server'
source '/opt/nolack/nolack-server/<ci_commit_sha>/nolack-server/venv/bin/activate'

python ui.py

echo "Running..."
python main.py
