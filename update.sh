set -e
trap 'last_command=$current_command; current_command=$BASH_COMMAND' DEBUG
trap 'echo "\"${last_command}\" command exited with exit code $?."' EXIT

echo "Nolack Server Runner Script"
echo "Version <ci_commit_sha> (<ci_commit_tag>)"
echo "        <ci_commit_title>"
if [ "$EUID" -ne 0 ]
  then echo "Please run the runner as root."
  exit
fi

echo "Updating Source..."
cd "opt/nolack/nolack-server/<ci_commit_sha>/nolack-server"
git checkout stable && 
git pull
apt update
apt install python3 python3-pip -y
apt upgrade
python3 -m pip install --upgrade virtualenv
source venv/bin/activate
